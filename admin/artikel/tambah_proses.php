<?php
require "../header.php";

$nama_artikel = $_POST['nama_artikel'];
$isi_artikel = $_POST['isi_artikel'];

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// pengecekan apakah berupa gambar
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check === false) {
        echo "<script>
                alert('File bukan sebuah gambar');
                document.location.href ='data_artikel.php';
            </script>";
        $uploadOk = 0;
    }
}

// pengecekan apakah file sudah ada
if (file_exists($target_file)) {
    echo "<script>
                alert('Maaf, file sudah ada');
                document.location.href ='data_artikel.php';
            </script>";
    $uploadOk = 0;
}

// pengecekan ukuran file
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "<script>
                alert('Maaf, file sangat besar');
                document.location.href ='data_artikel.php';
            </script>";
    $uploadOk = 0;
}
//pengecekan ekstensi gambar 
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "<script>
                alert('Maaf, hanya JPG, JPEG, PNG & GIF file yang diperbolehkan');
                document.location.href ='data_artikel.php';
            </script>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "<script>
                alert('Maaf, file tidak berhasil diupload');
                document.location.href ='data_artikel.php';
            </script>";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

        $sql="INSERT INTO artikel VALUES (NULL, '$nama_artikel', '$target_file', '$isi_artikel');";
        $proses = mysqli_query($koneksi, $sql);

        if ($proses){
        echo " <script>
                alert('Data Produk Berhasil di Tambahkan');
                document.location.href ='data_artikel.php';
            </script>";
        } else {
            echo "<script>
                alert('Data Produk Gagal di Tambahkan');
                document.location.href ='data_artikel.php';
            </script>";
        }
    } else {
        echo "<script>
                alert('Gambar Gagal di Upload');
                document.location.href ='data_artikel.php';
            </script>";
    }
}
?>